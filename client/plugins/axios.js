export default function({ $axios, redirect }) {

  $axios.onRequest((config) => {
    console.log('Making request to ' + config.url);
  })

  $axios.onError((error) => {
    const code = parseInt(error.response && error.response.status);
    console.log('error');
    console.log(error);
    if (code == 400) {
      redirect('/400');
    }
  })
}