export default function({ $axios, store, app }, inject) {
  const api = new API($axios, store);

  // asyncDataから呼び出すためにappに$apiを追加する
  app.$api = () => {
    return api;
  }
  inject('api', api);
}

class API {
  constructor(axios, store) {
    this.axios = axios;
    this.store = store; // ログイン状況を取得し、トークンをセットするためのstore
  }

  /** getメソッドを呼び出します */
  async get(url) {
    if (this.store.$auth.loggedIn) {
      this.axios.setHeader('Authorization', this.store.$auth.getToken('local'))
    }
    const res = await this.axios.$get(url);
    return res;
  }

  async getBlob(url) {
    if (this.store.$auth.loggedIn) {
      this.axios.setHeader('Authorization', this.store.$auth.getToken('local'))
    }
    const res = await this.axios.$get(url, {
      responseType: "blob"
    });
    return res;
   }

  /** postメソッドを呼び出します */
  async post(url, data) {
    if (this.store.$auth.loggedIn) {
      this.axios.setHeader('Authorization', this.store.$auth.getToken('local'))
    }
    const res = await this.axios.$post(url, data);
    return res;
  }
}