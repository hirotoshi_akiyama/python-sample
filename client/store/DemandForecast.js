export const state = () => ({
  imageUrl: null,
})

export const getters = {
  imageUrl: state => {
    return state.imageUrl;
  },
}

export const actions = {
  async getForecast({ commit }) {
    let response = await this.$api.getBlob('/api/v1/demand-forecast');
    commit('setForecastResult', response);
  }
}

export const mutations = {
  setForecastResult(state, response) {
    state.imageUrl = window.URL.createObjectURL(response);
    console.log('setForecastResult' + state.imageUrl);
  }
}
