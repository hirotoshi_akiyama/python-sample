/**
 * Menuリストを保存しておく変数
 */
export const state = () => ({
    loading: false,
    menuItems: {},
})

/**
 * Menuリストを返します
 */
export const getters = {
    menuItems: state => {
        return state.menuItems;
    },
}

/**
 * Menuリストをサーバーから取得します
 */
export const actions = {
    async getMenuList({ commit }) {
        // MenuItemsを取得する
        let result = await this.$axios.get('/menus');

        // storeに反映
        commit('setMenuList', result.data);
    }
}

export const mutations = {
    /**
     * ローディングの表示/非表示を切り替えます
     * @param state state 
     * @param payload ローディングの表示/非表示値（true/false） 
     */
    setLoading(state, payload) {
        state.loading = payload;
    },
    /**
     * Menuリストを保存します
     * @param state state 
     * @param menuItems メニューアイテムリスト
     */
    setMenuList(state, menuItems) {
        state.menuItems = menuItems;
    }
}
