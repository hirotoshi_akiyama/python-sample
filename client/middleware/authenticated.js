export default function ({ store, redirect, route }) {
  // ユーザーが認証されていないとき
  if (!store.$auth.loggedIn && route.path !== '/login') {
    return redirect('/login')
  }
}