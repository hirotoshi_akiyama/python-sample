INSERT 
INTO mst_user ( 
  user_code
  , user_name
  , user_type
  , mailaddress
  , password
  , disabled_at
  , version_no
  , created_user_id
  , created_at
  , updated_user_id
  , updated_at
) 
VALUES ( 
  'admin'
  , 'システム管理者'
  , '0'
  , 'test@be-forward.co.jp'
  , 'password'
  , null
  , 0
  , 1
  , current_timestamp
  , 1
  , current_timestamp
)