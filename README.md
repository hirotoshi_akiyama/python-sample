- 開発環境構築

1. Docker Desktopをインストール
2. VSCodeをインストール
3. Node.jsをインストール
4. GitからソースコードをClone


- Client環境

1. エクスプローラーからclinetフォルダを表示して、VSCodeで開きます。
2．VSCodeのターミナルを開き、「npm install」で必要なパッケージをインストールします。
3. ターミナルから「npm run dev」と打つとNuxtサーバーが起動します。

- Server環境

1. エクスプローラーからserverフォルダを表示して、VSCodeで開きます。
2. Serverプロジェクトを開いたVSCodeで拡張機能「Remote - Containers」をインストールします。
3. Ctl + Shift + Pを押してコマンドパレットを開き、「Reopen」と入力
4. 表示されたメニューから「Remote-Containers: Reopen in Container」を選択します。
5. Dockerファイルが読み込まれ、コンテナがビルドされたら
   F5キーでデバッグを開始するとPythonサーバーが起動します。

- DBにテストユーザーを登録
1. Serverプロジェクトのコンテナを起動させたら、Serverプロジェクト内のsqlフォルダにあるsqlをA5Mk2などで実行してmst_userテーブルを作成します。
2. gitリポジトリのrootにあるinsert_mst_user.sqlを実行しユーザーを作成します。<br/>
DB接続情報<br/>
サーバー：localhost<br/>
ポート：35432<br/>
データベース名：develop<br/>
ユーザー：dev01<br/>
パスワード：dev01<br/>


- ログイン
ユーザー：admin
パスワード:password


http://localhost:3000
にアクセスするとログイン画面が表示されます。


SwaggerUIでAPIの仕様を確認するには
http://localhost:5000/api/v1
にアクセス