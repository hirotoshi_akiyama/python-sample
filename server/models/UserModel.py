from datetime import datetime
from database import db, ma

# Userモデル


class User(db.Model):

    __tablename__ = 'mst_user'

    id = db.Column(db.Integer, primary_key=True)
    user_code = db.Column(db.String(20), nullable=False)
    user_name = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(20), nullable=True)
    user_type = db.Column(db.String(20), nullable=True)
    mailaddress = db.Column(db.String(50), nullable=True)
    disabled_at = db.Column(db.DateTime, nullable=False)
    version_no = db.Column(db.Integer, nullable=False, default=0)

    __mapper_args__ = {
        'version_id_col': version_no
    }


class UserSchema(ma.ModelSchema):
    class Meta:
        model = User

# ユーザー一覧のJSON返却用スキーマ定義


class UserListSchema(ma.ModelSchema):
    class Meta:
        fields = ('id', 'user_code', 'user_name', 'user_type', 'version_no')
