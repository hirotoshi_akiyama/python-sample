from datetime import datetime

def object_to_json(item):
  if isinstance(item, datetime):
    return item.isoformat()
  elif isinstance(item, object) and hasattr(item, '__dict__'):
    return item.__dict__
  else:
    raise TypeError
