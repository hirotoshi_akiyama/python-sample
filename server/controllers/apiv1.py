from flask import Blueprint
from flask_restx import Api
from controllers.user_controller import api as user
from controllers.demand_forecast_controller import api as demand_forcast

bp = Blueprint('apiv1', __name__, url_prefix='/api/v1')

# BlueprintをApiでラップする
api = Api(bp, title='B-gru WebAPI ver1.0', version='1.0',
          description='B-gru WebAPI ver1.0')

# controllerのapiをNamespaceとして追加する
api.add_namespace(user)
api.add_namespace(demand_forcast)
