from flask import make_response, jsonify, request
from flask_restx import Namespace, Resource, fields, reqparse
from flask_jwt import jwt_required, current_identity
from logging import getLogger
from sqlalchemy import and_, or_
from database import db
from datetime import datetime as dt
from utils.beanutil import object_to_json
import json
import models as model

# Namespaceの第一引数がエンドポイントとなる
api = Namespace('users', description='ユーザー情報を返すAPIです')


# strict_slashes:URL末尾のスラッシュを無効化
@api.route('/', strict_slashes=False)
class UserListController(Resource):
    @api.doc('ユーザー情報一覧を返します')
    @jwt_required()
    def get(self):
        # top500件を返す
        users = db.session.query(model.User).order_by(
            model.User.user_code).limit(500).all()
        return jsonify({'users': model.UserListSchema(many=True).dump(users)})

    @api.doc('ユーザー情報一覧を返します')
    @jwt_required()
    def post(self):
        filters = []

        if request.json is not None:
            # ユーザー名が検索条件として入力されている場合、
            # 検索条件に追加する
            user_name = request.json['user_name']
            if user_name is not None and len(user_name.strip()) > 0:
                user_name = '{}%'.format(user_name)
                filters.append(model.User.user_name.like(user_name))

            # 無効なユーザーを対象としない場合は
            # 削除日がnullでないユーザーを対象とする
            search_disabled = request.json['search_disabled']
            if search_disabled is not None and search_disabled == False:
                filters.append(model.User.disabled_at == None)

        users = db.session.query(model.User).filter(
            and_(*filters)).order_by(model.User.user_code).limit(500).all()
        return jsonify({'users': model.UserListSchema(many=True).dump(users)})


@api.route('/create')
class UserCreateController(Resource):
    @api.doc('ユーザー情報を登録します')
    @jwt_required()
    def post(self):
        '''
        ユーザーマスタ新規（登録ボタン）
        '''
        user = model.User()
        user.user_code = request.json['user_code']
        user.user_name = request.json['user_name']
        user.mailaddress = request.json['mailaddress']
        user.password = request.json['password']
        user.version_no = 0
        user.created_user_id = request.json['login_user_id']
        user.created_at = dt.now()
        user.updated_user_id = request.json['login_user_id']
        user.updated_at = dt.now()
        db.session.add(user)

        db.session.commit()

        return jsonify({'message': '登録しました。'})


@api.route('/edit/<int:id>')
class UserEditController(Resource):
    @api.doc('ユーザー情報を返します')
    @jwt_required()
    def get(self, id):
        '''
        ユーザーマスタ編集（画面遷移）
        '''
        user = db.session.query(model.User).get(id)
        return jsonify({'user': model.UserSchema().dump(user)})

    @api.doc('ユーザー情報を更新ます')
    @jwt_required()
    def post(self, id):
        '''
        ユーザーマスタ編集（登録ボタン）
        '''
        user = db.session.query(model.User).get(id)
        user.user_code = request.json['user_code']
        user.user_name = request.json['user_name']
        user.mailaddress = request.json['mailaddress']
        user.password = request.json['password']
        user.version_no = request.json['version_no']
        user.updated_user_id = request.json['login_user_id']
        user.updated_at = dt.now()

        db.session.commit()

        return jsonify({'message': '更新しました。'})


@api.route('/delete')
class UserDeleteController(Resource):
    @api.doc('ユーザー情報を削除します')
    @jwt_required()
    def post(self):
        '''
        ユーザーマスタ削除
        '''
        id = request.json['id']
        version_no = request.json['version_no']
        user = db.session.query(model.User).filter_by(
            id=id, version_no=version_no).first()
        if user:
            db.session.delete(user)
            db.session.commit()

        return jsonify({'message': '削除しました。'})
