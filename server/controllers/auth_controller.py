from flask import Blueprint, jsonify
from flask_jwt import jwt_required, current_identity
from werkzeug.security import safe_str_cmp
from logging import getLogger
from sqlalchemy import and_, or_
from database import db
import models as model

app = Blueprint('auth', __name__)


def authenticate(userCode: str, password: str) -> model.User:
    """
    引数userCode、passwordを受け取り、ユーザーマスター情報を返す関数

    引数
    -----
    userCode:str ユーザーコード
    password:str パスワード

    返り値
    -----
    ユーザーマスター情報
    """
    user = db.session.query(model.User).filter_by(
        user_code=userCode, disabled_at=None).first()
    if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
        return user


def identity(payload) -> model.User:
    """
    引数payloadを受け取り、ユーザーマスター情報を返す関数

    引数
    payload:object ペイロード

    返り値
    -----
    ユーザーマスター情報
    """
    user_id = payload['identity']
    return db.session.query(model.User).get(user_id)


@app.route('/me')
@jwt_required()
def me() -> model.UserSchema:
    """
    現在ログイン中のユーザーマスター情報をjson形式で返す関数

    返り値
    -----
    json文字列（ユーザーマスター情報:id, user_code, user_name, user_type）
    """
    return jsonify({'user': model.UserSchema().dump(current_identity)})


@app.route('/logout')
def logout():
    """
    ログアウト処理を行うための関数
    ※今のところ何もしないし、ログアウトボタンから呼ばれる予定でもないが一応定義しておく。
    　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　システム監査機能などを実装するときにログアウト処理を実装するか検討。
    """
    return jsonify({'success': True})
