from flask import make_response, jsonify, request
from flask_restx import Namespace, Resource, fields, reqparse
from flask_jwt import jwt_required, current_identity
from logging import getLogger
from utils.beanutil import object_to_json
from matplotlib import pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestRegressor
from io import BytesIO
import pandas as pd
import pickle
import json
import datetime as dt
import math


# Namespaceの第一引数がエンドポイントとなる
api = Namespace('demand-forecast',
                description='2017年の東京の電力消費量の予・実対比グラフを返すAPIです')


# strict_slashes:URL末尾のスラッシュ（/demand-forecast/→/demand-forecast）を無効化
@api.route('/', strict_slashes=False)
class DemandForcastController(Resource):
    @api.doc('ユーザー情報一覧を返します')
    @jwt_required()
    def get(self):
        # 保存した学習データを読み込み2017年の電力需要予測を行ってみる
        # 2017年の気象データを読み込み
        temp = pd.read_csv('data/temp_2017.csv', encoding="SHIFT-JIS",
                           skiprows=4).iloc[:, 0:2]
        temp.columns = ["DATE", "TEMP"]

        # 0列目のデータタイプをdatetime型に変換
        temp.index = temp.index.map(lambda x: dt.datetime.strptime(
            temp.loc[x].DATE, "%Y/%m/%d %H:%M:%S"))

        # 2016年12月31日からのデータが入っているので、2017年1月以降のデータを抽出
        temp = temp.query(
            "DATE >= '2017-01-01 00:00:00' and DATE < '2018-01-01 00:00:00'")

        # 月、曜日(月曜が0、日曜が6)、時間の列を作成
        temp['MONTH'] = temp.index.month
        temp['WEEK'] = temp.index.weekday
        temp['HOUR'] = temp.index.hour

        # 予測結果比較を行うために2017年の電力使用データを読み込む
        juyo = pd.read_csv('data/juyo-2017.csv',
                           encoding="SHIFT-JIS", skiprows=2)

        # 日付と時間を結合し、日付型に変換してインデックスに指定
        juyo.columns = ["DATE", "TIME", "KW"]
        juyo.index = juyo.index.map(lambda x: dt.datetime.strptime(
            juyo.loc[x].DATE + " " + juyo.loc[x].TIME, "%Y/%m/%d %H:%M"))

        # 2017年の電力データから実績値（電力量）のみ抽出
        y_test = juyo[["KW"]].values.astype('int').flatten()

        # 学習済みモデルを読み込み
        model = pickle.load(open('data/demand-forecast.csv', 'rb'))

        # 入力に使用するデータ列
        x_cols = ["MONTH", "WEEK", "HOUR", "TEMP"]
        x = temp[x_cols].values.astype('float')

        # 入力データを正規化（分散正規化）する
        scaler = StandardScaler()
        scaler.fit(x)
        x_test = scaler.transform(x)

        # 予測結果の取得
        result = model.predict(x_test)
        df_result = pd.DataFrame({
            "y_test": y_test,
            "result": result
        })

        # matplotlibでグラフの画像を作成する
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        df_result[:120].plot(ax=ax)

        # 作成したグラフの情報をcanvasに出力する
        canvas = FigureCanvas(fig)
        png_output = BytesIO()

        # 画像の情報をpngファイルとしてメモリに保存
        canvas.print_png(png_output)
        data = png_output.getvalue()

        # pngファイルをHTTPレスポンスに変換
        response = make_response(data)
        response.headers['Content-Type'] = 'image/png'
        response.headers['Content-Length'] = len(data)

        return response
