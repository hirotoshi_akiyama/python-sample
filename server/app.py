import os
import yaml
import logging
import logging.config
from flask import Flask
from flask_cors import CORS
from flask_jwt import JWT, jwt_required, current_identity
from database import init_db
import controllers.auth_controller
from controllers import apiv1

# configファイルの種類
config_type = {
    "development": "config.Development",
    "staging": "config.Staging",
    "production": "config.Production",
}

# logconfigファイル名
ENVKEY_LOGCONFIG = 'log_config'

# Flask アプリケーションの初期化


def create_app():
    """
    Flaskアプリケーションの初期化関数
    loggingやデータベース接続文字列などのconfig情報を読み込む
    """
    app = Flask(__name__)

    # logging initialize
    with open('logging-config.yml', 'r', encoding='utf-8') as f:
        env_data = yaml.safe_load(f)
    logging.config.dictConfig(env_data[ENVKEY_LOGCONFIG])
    # SqlAlchemyのログ出力を追加
    logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)

    # load config.py
    # 環境変数FLASK_APP_ENVはlaunch.jsonにて設定
    app.config.from_object(config_type.get(
        os.getenv('FLASK_APP_ENV', 'production')))

    # database initialize
    init_db(app)

    return app


# Flaskアプリケーションの初期化
app = create_app()

# Cross Origin Resource Sharing対策
CORS(app)

# 認証
jwt = JWT(app, controllers.auth_controller.authenticate,
          controllers.auth_controller.identity)

# blueprintの追加
# authはjwt認証するためにrestxの枠から外すためAPIとは別でblueprintを追加する
app.register_blueprint(controllers.auth_controller.app)
# apiv1.pyで登録されたNamespaceをBlueprintに追加
app.register_blueprint(apiv1.bp)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
