import os
from datetime import timedelta

class Development:
    """
    開発環境設定
    """
    # Flask
    DEBUG = True
    # SQLAlchemy
    SQLALCHEMY_DATABASE_URI = 'postgresql://dev01:dev01@sample-db:5432/develop'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = True
    SECRET_KEY = 'develop-secret'
    # Flask-JWT
    # 認証用のユーザーコード名
    JWT_AUTH_USERNAME_KEY = 'userCode'
    # Authenticationヘッダーのエンドポイント名
    JWT_AUTH_HEADER_PREFIX = 'Bearer'
    # 有効期限（秒数/初期値300秒）
    JWT_EXPIRATION_DELTA = timedelta(hours=2)

class Staging:
    """
    検証環境設定情報
    """
    # SQLAlchemy
    SQLALCHEMY_DATABASE_URI = ''
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False
    SECRET_KEY = 'staging-secret'
    # Flask-JWT
    # 認証用のユーザーコード名
    JWT_AUTH_USERNAME_KEY = 'userCode'
    # Authenticationヘッダーのエンドポイント名
    JWT_AUTH_HEADER_PREFIX = 'Bearer'
    # 有効期限（秒数/初期値300秒）
    JWT_EXPIRATION_DELTA = timedelta(hours=12)  # 12時間

class Production:
    """
    本番環境設定情報
    """
    # SQLAlchemy
    SQLALCHEMY_DATABASE_URI = ''
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False
    SECRET_KEY = 'production-secret'
    # Flask-JWT
    # 認証用のユーザーコード名
    JWT_AUTH_USERNAME_KEY = 'userCode'
    # Authenticationヘッダーのエンドポイント名
    JWT_AUTH_HEADER_PREFIX = 'Bearer'
    # 有効期限（秒数/初期値300秒）
    JWT_EXPIRATION_DELTA = timedelta(hours=12)  # 12時間
