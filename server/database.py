from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
# from models.BaseModel import BgruBaseModel
from datetime import datetime
import sqlalchemy as sa


class BaseModel(object):
    created_user_id = sa.Column(sa.Integer, nullable=True)
    created_at = sa.Column(sa.DateTime, nullable=True, default=datetime.now)
    updated_user_id = sa.Column(sa.Integer, nullable=True)
    updated_at = sa.Column(sa.DateTime, nullable=True, default=datetime.now)


# SQLAlchemyクラスのインスタンスを生成する時にmodel_classを指定するとModelの基底クラスを指定できる
db = SQLAlchemy(model_class=BaseModel)

# Model↔jsonの変換を可能にするクラスを初期化
ma = Marshmallow()

# SQLAlchemyの初期化


def init_db(app):
    db.init_app(app)
