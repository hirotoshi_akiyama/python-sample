-- Project Name : Smart-EDI
-- Date/Time    : 2020/03/22 23:51:22
-- Author       : hirot
-- RDBMS Type   : PostgreSQL
-- Application  : A5:SQL Mk-2

/*
  BackupToTempTable, RestoreFromTempTable疑似命令が付加されています。
  これにより、drop table, create table 後もデータが残ります。
  この機能は一時的に $$TableName のような一時テーブルを作成します。
*/

-- ユーザーマスター
--* RestoreFromTempTable
create table mst_user (
  id serial not null
  , user_code varchar(20) not null
  , user_name varchar(50) not null
  , user_type varchar(20)
  , mailaddress varchar(50)
  , password varchar(20) not null
  , disabled_at timestamp
  , version_no int not null
  , created_user_id int not null
  , created_at timestamp not null
  , updated_user_id int
  , updated_at timestamp not null
  , constraint mst_user_PKC primary key (id)
) ;

create unique index mst_user_IX1
  on mst_user(user_code);

comment on table mst_user is 'ユーザーマスター';
comment on column mst_user.id is 'ID';
comment on column mst_user.user_code is 'ユーザーコード';
comment on column mst_user.user_name is '名前';
comment on column mst_user.user_type is 'ユーザー種別';
comment on column mst_user.mailaddress is 'メールアドレス';
comment on column mst_user.password is 'パスワード';
comment on column mst_user.disabled_at is '削除日';
comment on column mst_user.version_no is 'バージョン番号';
comment on column mst_user.created_user_id is '登録者ID';
comment on column mst_user.created_at is '登録日時';
comment on column mst_user.updated_user_id is '更新者ID';
comment on column mst_user.updated_at is '更新日時';
